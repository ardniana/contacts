Contacts
========


Update database (restore from dump)
===================================
1. Go to folder mysql inside project
2. Run: $ ./update_db.sh


Getting access to database
==========================
1. Run: $ mysql -u contacts -p


Migration
=========
1. Make new migration: $ python manage.py makemigrations
2. Apply it: $ python manage.py migrate


Start web server
================
1. Run server:
    $ python manage.py runserver
