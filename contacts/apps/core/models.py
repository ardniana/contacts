# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from contacts.apps.accounts.models import UserProfile
from datetime import datetime


# Create your models here.
class Contacts(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    contact_name = models.CharField(max_length=30, blank=False)
    contact_surname = models.CharField(max_length=30, blank=False)
    city_name = models.CharField(max_length=30, blank=True)
    country_name = models.CharField(max_length=30, blank=True)
    phone_number = models.CharField(max_length=15, blank=False)
    email_contact = models.EmailField(blank=False)
    date_birthday = models.DateField(null=False, blank=True)
    date_add = models.DateField(default=datetime.now())
