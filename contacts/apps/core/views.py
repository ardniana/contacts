# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import json
import os
import csv
import requests
from django.shortcuts import render
from django.views.generic import View
from django.contrib.auth.models import User
from contacts.apps.core.models import Contacts
from contacts.apps.accounts.models import UserProfile
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from datetime import datetime, timedelta, date
from contacts.apps.core.forms import ContactsAddForm
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from contacts.settings import BASE_DIR
from django.core.validators import validate_email
from django.core.exceptions import ValidationError


# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def email(value):
    try:
        validate_email(value)
        return True
    except (ValidationError), e:
        return False


def date_validate(date_text):
    try:
        datetime.strptime(date_text, '%Y-%m-%d')
        return True
    except ValueError:
        return False


class DashBoardPage(View):
    template_name = 'dashboard.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_superuser:
            user_profile = UserProfile.objects.filter(user=request.user)
            data = {
                'count_of_all_users': User.objects.all().count(),
                'count_of_all_contacts': Contacts.objects.filter(user_profile=user_profile).count()
            }
        else:
            user_profile = UserProfile.objects.filter(user=request.user)
            data = {
                'count_of_all_contacts': Contacts.objects.filter(user_profile=user_profile).count()
            }
        return render(request, self.template_name, data)


class AllContacts(View):
    template_name = 'all_contacts.html'

    def get(self, request, *args, **kwargs):
        active_city = []
        active_country = []
        if request.user.is_superuser:
            contacts_filter = Contacts.objects.all().order_by('-id')
        else:
            user_profile = UserProfile.objects.filter(user=request.user)
            contacts_filter = Contacts.objects.filter(user_profile=user_profile)
        for row in contacts_filter:
            if not row.city_name in active_city:
                active_city.append(row.city_name)
            if not row.country_name in active_country:
                active_country.append(row.country_name)
        data = {
            'city_name': active_city,
            'country_name': active_country,
            'today': datetime.today().strftime('%Y-%m-%d')
        }
        return render(request, self.template_name, data)


def ajax_get_all_contacts(request):
    user_profile = UserProfile.objects.get(user=request.user)
    ajax_response = {'sEcho': '', 'aaData': [], 'iTotalRecords': 0, 'iTotalDisplayRecords': 0}
    try:
        ajax_type = request.GET['type']
    except MultiValueDictKeyError:
        ajax_type = ''
    if ajax_type == 'dashboard':
        contacts = Contacts.objects.filter(user_profile=user_profile).order_by('contact_name')[:10]
    else:
        if request.user.is_superuser:
            contacts = Contacts.objects.all().order_by('-id')
        else:
            contacts = Contacts.objects.filter(user_profile=user_profile).order_by('-contact_name')
    # filters
    city_filter = request.GET.get('sSearch_0')
    country_filter = request.GET.get('sSearch_1')
    if city_filter and city_filter != "0":
        contacts = contacts.filter(city_name=city_filter)
    if country_filter and country_filter != "0":
        contacts = contacts.filter(country_name=country_filter)
    sort_data = request.GET.get('iSortCol_0')
    metod_sort = request.GET.get('sSortDir_0')
    char_sort = ''
    if metod_sort == 'desc': char_sort = '-'
    if sort_data == '8':
        contacts = contacts.order_by(char_sort+'date_add')
    # getting lens of response
    ajax_response['iTotalRecords'] = len(contacts)
    ajax_response['iTotalDisplayRecords'] = len(contacts)
    if ajax_type == 'dashboard':
        for contact in contacts:
            ajax_response['aaData'].append([
                contact.id,
                contact.contact_name,
                contact.contact_surname,
                contact.city_name,
                contact.country_name,
                contact.phone_number,
                contact.email_contact,
                contact.date_birthday.strftime('%Y-%m-%d'),
                contact.date_add.strftime('%Y-%m-%d')
            ])
    else:
        start = int(request.GET['iDisplayStart'])
        length = int(request.GET['iDisplayLength'])
        for contact in contacts[start:start + length]:
            if request.user.is_superuser:
                user = str(contact.user_profile)
            else:
                user = ''
            ajax_response['aaData'].append([
                contact.id,
                contact.contact_name,
                contact.contact_surname,
                contact.city_name,
                contact.country_name,
                contact.phone_number,
                contact.email_contact,
                contact.date_birthday.strftime('%Y-%m-%d'),
                contact.date_add.strftime('%Y-%m-%d'),
                user
            ])
    return HttpResponse(json.dumps(ajax_response), content_type='application/json')


class AjaxContactChangeModal(View):

    def get(self, request, *args, **kwargs):
        contact_id = int(request.GET['contact_id'])
        contact = Contacts.objects.get(pk=contact_id)
        ajax_response = {
                        "contact_id": contact_id,
                         "contact_name": contact.contact_name,
                         "contact_surname": contact.contact_surname,
                         "city": contact.city_name,
                         "country": contact.country_name,
                         "phone": contact.phone_number,
                         "email": contact.email_contact,
                         "birthday": contact.date_birthday.strftime('%Y-%m-%d'),
                         "date_add": contact.date_add.strftime('%Y-%m-%d')
                         }
        return HttpResponse(json.dumps(ajax_response), content_type='application/json')

    def post(self, request, *args, **kwargs):
        form = ContactsAddForm(request.POST)
        if form.is_valid():
            contact_data = {'contact_name': form.cleaned_data['contact_name'],
                            'contact_surname': form.cleaned_data['contact_surname'],
                            'city_name':form.cleaned_data['contact_city'],
                            'country_name':form.cleaned_data['contact_country'],
                            'phone_number':form.cleaned_data['contact_phone'],
                            'email_contact':form.cleaned_data['contact_email'],
                            'date_birthday':form.cleaned_data['contact_b_date'],
                            }
            Contacts.objects.filter(pk=form.cleaned_data['contact_id']).update(**contact_data)

            return HttpResponse(json.dumps({'success': True}),
                                content_type="application/json")
        else:
            return HttpResponse(json.dumps({'success': False,
                                            'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                content_type="application/json")


def ajax_delete_contact_modal(request):
    if request.method == "POST" and request.is_ajax:
        contact = Contacts.objects.get(pk=request.POST['contact_id'])
        contact.delete()
        return HttpResponse(json.dumps({'success': True}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({'success': False}), content_type="application/json")

@csrf_exempt
def ajax_add_contact_modal(request):

    if request.method == "GET" and request.is_ajax:
        form = ContactsAddForm()

        return HttpResponse(json.dumps({'success': True}),
                            content_type="application/json")

    if request.method == "POST" and request.is_ajax:
        form = ContactsAddForm(request.POST)
        if form.is_valid():
            user_profile = UserProfile.objects.get(user=request.user)
            Contacts.objects.create(contact_name=form.cleaned_data['contact_name'],
                                    contact_surname=form.cleaned_data['contact_surname'],
                                    city_name=form.cleaned_data['contact_city'],
                                    country_name=form.cleaned_data['contact_country'],
                                    phone_number=form.cleaned_data['contact_phone'],
                                    email_contact=form.cleaned_data['contact_email'],
                                    date_birthday=form.cleaned_data['contact_b_date'],
                                    date_add=form.cleaned_data['contact_add_date'],
                                    user_profile=user_profile
                                    )
            return HttpResponse(json.dumps({'success': True}),
                                content_type="application/json")
        else:
            return HttpResponse(json.dumps({'success': False,
                                            'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                content_type="application/json")
    else:
        return HttpResponse(json.dumps({'success': False}), content_type="application/json")

@csrf_exempt
def upload_csv_file(request):
    if request.method == 'POST':
        csvfile = request.FILES['csvFile']
        fs = FileSystemStorage()
        filename = fs.save('contacts_%s.csv' % str(Contacts.objects.all().last().id + 1), csvfile)
        uploaded_file_url = fs.url(filename)
        return HttpResponse(json.dumps({'file': uploaded_file_url}))
    else:
        return HttpResponse(json.dumps({'error': 'Something wrong'}))


def ajax_add_contact_csv(request):
    user_profile = UserProfile.objects.get(user=request.user)
    input_file = open(BASE_DIR + '/contacts/media/contacts_%s.csv' % str(Contacts.objects.all().last().id + 1), 'rb')
    csv_contacts = csv.DictReader(input_file, fieldnames=['contact_name', 'contact_surname', 'city_name',
                                                          'country_name', 'phone_number', 'email_contact',
                                                          'date_birthday'])
    i = 0
    errors = {}
    success = []
    for row in csv_contacts:
        i += 1
        error = []
        if row['contact_name'] and row['contact_surname'] and row['email_contact']:
            if not email(row['email_contact']):
                error.append(['Email error'])
            if len(row['phone_number']) > 15:
                error.append(['Phone symbol error'])
            if not date_validate(row['date_birthday']):
                error.append(['Incorrect data format, should be YYYY-MM-DD'])
        else:
            error.append(['Input error'])
        if len(error) > 0:
            errors[i] = error
        else:
            contact_data = Contacts(contact_name=row['contact_surname'],
                                    contact_surname=row['contact_surname'],
                                    city_name=row['city_name'],
                                    country_name=row['country_name'],
                                    phone_number=row['phone_number'],
                                    email_contact=row['email_contact'],
                                    date_birthday=row['date_birthday'],
                                    date_add=datetime.today().strftime('%Y-%m-%d'),
                                    user_profile=user_profile)
            contact_data.save()
            success.append(i)
            # Contacts.objects.bulk_create(contact_data)
    return HttpResponse(json.dumps({'success': success,
                                    'errors': len(errors),
                                    }),
                        content_type="application/json")


def ajax_add_contact_vero(request):
    if request.method == 'GET':
        user_profile = UserProfile.objects.get(user=request.user)
        user_api = user_profile.vero_api_key
        contacts = Contacts.objects.filter(user_profile=user_profile)
        for contact in contacts:
            requests.request('POST', 'https://api.getvero.com/api/v2/users/track',
                             json={'auth_token': user_api, 'id': contact.id, 'email': contact.email_contact,
                                   'data': {
                                       'first_name': contact.contact_name,
                                       'last_name': contact.contact_surname,
                                       'country': contact.country_name,
                                       'city': contact.city_name,
                                       'phone': contact.phone_number,
                                       'date birthday': contact.date_birthday.strftime('%Y-%m-%d'),
                                   }
                                   })
        return HttpResponse(json.dumps({'success': True}),
                            content_type="application/json")


class AllUsers(View):
    template_name = 'all_users.html'

    def get(self, request, *args, **kwargs):
        data = {}
        return render(request, self.template_name, data)


def ajax_clients_datatable(request):
    if request.user.is_superuser:
        ajax_response = {'sEcho': '', 'aaData': [], 'iTotalRecords': 0, 'iTotalDisplayRecords': 0}
        ajax_response['iTotalRecords'] = len(User.objects.all())
        ajax_response['iTotalDisplayRecords'] = len(User.objects.all())
        for user in User.objects.all():
            ajax_response["aaData"].append([
                user.pk,
                user.username,
                user.first_name,
                user.last_name,
                user.email,
                user.profile.phone_number,
                '*********%s' % user.profile.vero_api_key[-4:]
            ])
    else:
        ajax_response = {"aaData": []}
    return HttpResponse(json.dumps(ajax_response), content_type='application/json')
