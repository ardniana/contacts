from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from contacts.apps.core.views import DashBoardPage, AllContacts, ajax_get_all_contacts, \
    AjaxContactChangeModal, ajax_delete_contact_modal, ajax_add_contact_modal, upload_csv_file, \
    ajax_add_contact_csv, ajax_add_contact_vero, AllUsers, ajax_clients_datatable

urlpatterns = [
    url(r'^$', login_required(DashBoardPage.as_view()), name='dashboard'),
    url(r'^all_contacts/$', login_required(AllContacts.as_view()), name='all_contacts'),
    url(r'^all_users/$', login_required(AllUsers.as_view()), name='all_users'),
    url(r'^ajax_get_all_contacts/$', login_required(ajax_get_all_contacts), name='ajax_get_all_contacts'),
    url(r'^ajax_clients_datatable/$', login_required(ajax_clients_datatable), name='ajax_clients_datatable'),
    url(r'^ajax_contact_change_modal/$', login_required(AjaxContactChangeModal.as_view()),
        name='ajax_contact_change_modal'),
    url(r'^ajax_delete_contact_modal/$', login_required(ajax_delete_contact_modal), name='ajax_delete_contact_modal'),
    url(r'^ajax_add_contact_modal/$', login_required(ajax_add_contact_modal), name='ajax_add_contact_modal'),
    url(r'^upload_csv_file/$', login_required(upload_csv_file), name='upload_csv_file'),
    url(r'^ajax_add_contact_csv/$', login_required(ajax_add_contact_csv), name='ajax_add_contact_csv'),
    url(r'^ajax_add_contact_vero/$', login_required(ajax_add_contact_vero), name='ajax_add_contact_vero'),
]