from django import forms
from contacts.apps.core.models import Contacts
from contacts.apps.accounts.models import UserProfile


class ContactsAddForm(forms.Form):
    contact_id = forms.IntegerField()
    contact_name = forms.CharField(max_length=30, required=True)
    contact_surname = forms.CharField(max_length=30, required=True)
    contact_city = forms.CharField(max_length=30, required=False)
    contact_country = forms.CharField(max_length=30, required=False)
    contact_phone = forms.CharField(max_length=15, required=False)
    contact_email = forms.EmailField(required=True)
    contact_b_date = forms.DateField(required=False)
    contact_add_date = forms.DateField(required=True)

    def clean(self):
        form_data = self.cleaned_data
        return form_data