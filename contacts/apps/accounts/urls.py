from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from contacts.apps.accounts.views import LoginPage, SettingsPage, ChangePasswordView, RegisterFormView

urlpatterns = [
    url(r'^login/$', LoginPage.as_view(), name='login'),
    url(r'^register/$', RegisterFormView.as_view(), name='register'),
    url(r'^settings/$', login_required(SettingsPage.as_view()), name='settings'),
    url(r'^change_password/$',login_required(ChangePasswordView.as_view()),name='change_password'),

]