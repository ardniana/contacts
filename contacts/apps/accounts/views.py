# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
import json
import logging
import traceback
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseServerError, HttpResponseRedirect
from django.views.generic import View
from django.views.generic.edit import FormView
from contacts.apps.accounts.forms import SettingsForm, ChangePasswordForm, SignupForm

from contacts.apps.accounts.models import UserProfile

logger = logging.getLogger("contacts")


class LoginPage(View):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('core:dashboard'))
        return render(request, self.template_name, {'error': False})

    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']
        logger.debug('Authenticating user %s with password: %s' % (username, password))
        if User.objects.filter(email=username):
            username = User.objects.get(email=username).username
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('core:dashboard'))
        else:
            logger.debug("User not authenticated. Wrong login(%s)/password(%s)." % (request.POST['username'],
                                                                                    request.POST['password']))
            return render(request, self.template_name, {'error': True,
                                                        'message': 'User not authenticated. Wrong login/password'})


class SettingsPage(View):
    template_name = 'settings.html'

    def get(self, request, *args, **kwargs):
        try:
            form = SettingsForm(request.user.profile, instance=request.user.profile)
        except Exception as e:
            logger.error("Exception: %s. Trace: %s." % (e, traceback.format_exc(limit=10)))
        else:
            return render(request, self.template_name, {"form": form,
                                                        "updated": False,
                                                        })

    def post(self, request, *args, **kwargs):
        try:
            updated = False
            form = SettingsForm(None, request.POST, instance=request.user.profile)
            if form.is_valid():
                user = User.objects.get(id=request.user.pk)
                user.username = form.cleaned_data['username']
                user.first_name = form.cleaned_data['first_name']
                user.last_name = form.cleaned_data['last_name']
                user.email = form.cleaned_data['email']
                user.save()
                user_profile = UserProfile.objects.get(user=request.user)
                user_profile.phone_number = form.cleaned_data['phone_number']
                user_profile.save()
                updated = True
        except Exception as e:
            logger.error("Exception: %s. Trace: %s." % (e, traceback.format_exc(limit=10)))
            return HttpResponseServerError()
        else:
            return render(request, self.template_name, {"form": form,
                                                        "updated": updated,
                                                        })


class ChangePasswordView(View):
    template_name = 'change_password_form.html'

    def get(self, request, *args, **kwargs):
        try:
            data = {}
        except Exception as e:
            logger.error("Exception: %s. Trace: %s." % (e, traceback.format_exc(limit=10)))
        else:
            return render(request, self.template_name, data)

    def post(self, request, *args, **kwargs):
        try:
            form = ChangePasswordForm(request.POST, instance=request.user)
            if form.is_valid():
                user = request.user
                user.set_password(form.cleaned_data['new_password'])
                user.save()
                return HttpResponse(json.dumps({'success': True}),
                                    content_type="application/json")
            else:
                return HttpResponse(json.dumps({'success': False,
                                                'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                    content_type="application/json")
        except Exception as e:
            logger.error("Exception: %s. Trace: %s." % (e, traceback.format_exc(limit=10)))


class RegisterFormView(FormView):
    template_name = "register.html"

    def get(self, request, *args, **kwargs):
        form = SignupForm()
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        updated = False
        try:
            form = SignupForm(request.POST)
            if form.is_valid():
                user = User.objects.create(username=form.cleaned_data['username'],
                                           first_name=form.cleaned_data['first_name'],
                                           last_name=form.cleaned_data['last_name'],
                                           email=form.cleaned_data['email'],
                                           )
                user.set_password(form.cleaned_data['password1'])
                user.save()
                UserProfile.objects.create(
                    user=user,
                    phone_number=form.cleaned_data['phone_number'],
                    vero_api_key=form.cleaned_data['vero_api_key'],
                )
                updated = True
                template_name1 = 'login.html'
                return render(request, template_name1, {"updated": updated,})
                # return HttpResponseRedirect(reverse('accounts:login'))
        except Exception as e:
            logger.error("Exception: %s. Trace: %s." % (e, traceback.format_exc(limit=10)))
        else:
            return render(request, self.template_name, {"form": form,
                                                       })
