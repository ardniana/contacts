import re
from django import forms
import requests
from contacts.apps.core.models import Contacts
from contacts.apps.accounts.models import UserProfile
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.contrib.auth import authenticate


class SettingsForm(ModelForm):
    class Meta:
        model = UserProfile
        fields = []

    username = forms.CharField(max_length=30, required=True)
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(required=True)
    phone_number = forms.CharField(max_length=15, required=True)
    vero_api_key_readonly = forms.CharField()

    def __init__(self, profile, *args, **kwargs):
        super(SettingsForm, self).__init__(*args, **kwargs)
        if profile:
            self.fields['username'].initial = profile.user.username
            self.fields['first_name'].initial = profile.user.first_name
            self.fields['last_name'].initial = profile.user.last_name
            self.fields['email'].initial = profile.user.email
            self.fields['phone_number'].initial = profile.phone_number
        self.fields['vero_api_key_readonly'].initial = '*********%s' % self.instance.vero_api_key[-4:]

    def clean(self):
        form_data = super(SettingsForm, self).clean()
        if not self.is_valid():
            return form_data
        if form_data['username'] != self.instance.user.username and User.objects.filter(username=form_data['username']):
            self._errors["username"] = self.error_class(["User with this username already exists."])
        if form_data['email'] != self.instance.user.email and User.objects.filter(email=form_data['email']):
            self._errors["email"] = self.error_class(["User with this email already exists."])
        return form_data


class ChangePasswordForm(ModelForm):
    class Meta:
        model = User
        fields = []

    old_password = forms.CharField(required=True)
    new_password = forms.CharField(required=True)
    password_confirmation = forms.CharField(required=True)

    def clean(self):
        form_data = self.cleaned_data
        if not 'old_password' in form_data or not 'new_password' in form_data \
                or not 'password_confirmation' in form_data:
            self._errors["old_password"] = self.error_class(["Field is required"])
            self._errors["new_password"] = self.error_class(["Field is required"])
            self._errors["password_confirmation"] = self.error_class(["Field is required"])
            return form_data
        user = authenticate(username=self.instance.username, password=form_data['old_password'])
        if user is None:
            self._errors["old_password"] = self.error_class(["Wrong password"])
        if form_data['new_password'] != form_data["password_confirmation"]:
            self._errors["new_password"] = self.error_class(["Passwords don\'t match"])
            self._errors["password_confirmation"] = self.error_class(["Passwords don\'t match"])
        if not re.match(r'[A-Za-z0-9@#$%^&+=]{8,32}', form_data['new_password']):
            self._errors["new_password"] = self.error_class(["Password must contain uppercase and lowercase "
                                                             "letters and has at least eight (8) characters "
                                                             "but no more than thirty two (32) characters."])
        return form_data


class SignupForm(forms.Form):
    class Meta:
        model = User
        fields = []

    username = forms.CharField(label="User Name", required=True)
    first_name = forms.CharField(label="First Name", required=True)
    last_name = forms.CharField(label="Last Name", required=True)
    email = forms.EmailField(required=True)
    password1 = forms.CharField(required=True)
    password2 = forms.CharField(required=True)
    phone_number = forms.CharField(label="Phone Number", required=True)
    vero_api_key = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)

    def clean(self):
        form_data = super(SignupForm, self).clean()
        no_required_data = False
        if not self.is_valid():
            no_required_data = True

        if self.cleaned_data.get('username', None) and User.objects.filter(username=form_data['username']):
            self._errors["username"] = self.error_class(["User with this username already exists."])
            no_required_data = True
        if self.cleaned_data.get('email', None) and User.objects.filter(email=form_data['email']):
            self._errors["email"] = self.error_class(["User with this email already exists."])
            no_required_data = True
        if self.cleaned_data.get('password1', None) and \
                self.cleaned_data.get('password2', None) and form_data['password1'] != form_data["password2"]:
            self._errors["password1"] = self.error_class(["Passwords don\'t match"])
            self._errors["password2"] = self.error_class(["Passwords don\'t match"])
            no_required_data = True
        if self.cleaned_data.get('password1', None) and \
                not re.match(r'[A-Za-z0-9@#$%^&+=]{8,32}', form_data['password1']):
            self._errors["password1"] = self.error_class(["Password must contain uppercase and lowercase "
                                                          "letters and has at least eight (8) characters "
                                                          "but no more than thirty two (32) characters."])
            no_required_data = True
        if self.cleaned_data.get('vero_api_key', None):
            r = requests.request('POST', 'https://api.getvero.com/api/v2/users/track',
                             json={'auth_token': form_data['vero_api_key'], 'id': 'test', 'email': '',
                                   })
            if r.status_code == 401:
                self._errors["vero_api_key"] = self.error_class(["Your credentials are invalid!!!"])
                no_required_data = True
        if no_required_data == True:
            return form_data

