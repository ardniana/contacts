/**
 * Created by sasha on 22.05.17.
 */
$(document).ready(function() {
    var key_modal;
    var allContactsTable = $('#allContactsTable').dataTable({
        "bServerSide": true,
        "sAjaxSource": links.get_all_contacts,
        "bProcessing": true,
        "bLengthChange": true,
        "bFilter": true,
        'sDom': 'rtpli',
        "bSortable": true,
        "autoWidth": true,
        "bInfo": true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
        "iDisplayLength": 10,
        "aoColumnDefs": [
            {
                "mRender": function (data, type, row) {
                    if (data != '') {
                        $('#user_row').show();
                        return data
                    }
                    else {
                        return data
                    }
                },
                "aTargets": [9]
            },
            {
                "mRender": function (data, type, row) {
                    row.status_filter = '<button type="button" style="border-radius: 0" class="btn btn-primary btn-sm edit_modal" data-cid="' + row[0] + '" data-toggle="modal" data-target="#contact_modal">' +
                            'EDIT</button></div>';

                    return row.status_filter;
                },
                "aTargets": [10],
                "orderable": false
            },
            {
                "aTargets": [0, 1, 2, 3, 4, 5, 6, 7, 9],
                "bSortable": false
            },
        ]
    });


    $('#country-filter', this).change( function () {
         allContactsTable.fnFilter($(this).val(), 1);
    });

    $('#city-filter', this).change( function () {
         allContactsTable.fnFilter($(this).val(), 0);
    });

    $('#contact_modal').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var cid = button.data('cid');
        $('#contact_add_date').prop('disabled', true);
        $("#contact_b_date").datepicker({
                        startDate: '-100y',
                        endDate: '1'
                    });
        $("#contact_b_date").datepicker('formatDate', "yy-mm-dd").datepicker('setDate', "-50y");
        $('#contact_add_date').datepicker('setDate', "{0}d");
        $('#contact_name_div, #contact_surname_div, #contact_city_div, #contact_country_div, ' +
                '#contact_phone_div, #contact_email_div, ' +
                '#contact_b_date_div, #contact_add_date_div').removeClass('has-error');
        $('#contact_name_help, #contact_surname_help, #contact_city_help, #contact_country_help, ' +
                '#contact_phone_help, #contact_email_help, #contact_b_date_help, #contact_add_date_help').text('');
        if (cid != null) {
            key_modal = true;
            $('#delete_contact').show();
            $('#contact_title_modal').text('Manage Contacts Form')
            $.ajax({
                type: "GET",
                url: links.ajax_contact_change_modal,
                data: {
                    'contact_id': cid
                },
                success: function (data) {
                    $('#contact_id').val(data['contact_id']);
                    $('#contact_name').val(data['contact_name']);
                    $('#contact_surname').val(data['contact_surname']);
                    $('#contact_city').val(data['city']);
                    $('#contact_country').val(data['country']);
                    $('#contact_phone').val(data['phone']);
                    $('#contact_email').val(data['email']);
                    $('#contact_b_date').val(data['birthday']);
                    $('#contact_add_date').val(data['date_add']);
                },
            });
        }
        if (cid==null){
            key_modal = false;
            $('#delete_contact').hide();
            $('#contact_name, #contact_surname, #contact_city, ' +
                '#contact_country, #contact_phone, #contact_email').val(null);
            $('#contact_id').val(0)
            $('#contact_title_modal').text('Add Contacts Form');
        }
    });

    $("#delete_contact").click(function () {
        if (confirm("Are you sure, that you want delete this version report?")) {
            var data = {'contact_id': $('#contact_id').val()};
            var csrftoken = getCookie('csrftoken');
            $.ajax({
                beforeSend: function (xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                type: "POST",
                url: links.ajax_delete_contact_modal,
                data: data,
                success: function (data) {
                    if (data['success']) {
                        alert('Deleted!');
                        $('#contact_modal').modal('hide');
                         allContactsTable.api().ajax.reload(null, false);
                    } else {
                        alert('Not deleted!');
                    }
                },
            });
        }
    });

    $("#new_contact").click(function () {
        var data = {
            'contact_id': $('#contact_id').val(),
            'contact_name': $('#contact_name').val(),
            'contact_surname': $('#contact_surname').val(),
            'contact_city': $('#contact_city').val(),
            'contact_country': $('#contact_country').val(),
            'contact_phone': $('#contact_phone').val(),
            'contact_email': $('#contact_email').val(),
            'contact_b_date': $('#contact_b_date').val(),
            'contact_add_date': $('#contact_add_date').val()
        };
        if (key_modal==false) {
            $.ajax({
                type: "POST",
                url: links.ajax_add_contact_modal,
                data: data,
                success: function (data) {
                    if (!(data['success'])) {
                        data['errors'].forEach(function (item, i, arr) {
                            $('#' + item[0] + '_div').addClass('has-error');
                            $('#' + item[0] + '_help').text(item[1]);
                        });
                        alert('Not add new contact.')
                    } else {
                        alert('Contact saved.');
                        $('#contact_modal').modal('hide');
                        allContactsTable.api().ajax.reload(null, false);
                    }
                },
            });
        }
        else {
            var csrftoken = getCookie('csrftoken');
            $.ajax({
                beforeSend: function (xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                type: "POST",
                url: links.ajax_contact_change_modal,
                data: data,
                success: function (data) {
                    if (!(data['success'])) {
                        data['errors'].forEach(function (item, i, arr) {
                            $('#' + item[0] + '_div').addClass('has-error');
                            $('#' + item[0] + '_help').text(item[1]);
                        });
                        alert('Not add new contact.')
                    } else {
                        alert('Contact saved.');
                        $('#contact_modal').modal('hide');
                        allContactsTable.api().ajax.reload(null, false);
                    }
                },
            });
        }

      });
});


