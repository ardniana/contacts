/**
 * Created by sasha on 24.05.17.
 */
$(document).ready(function() {
    var contactTable;
    contactTable = $('#contactTable').dataTable({
        "ajax": {
            "url": links.get_all_contacts,
            "data": {
                'type': 'dashboard'
            }
        },
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "bSortable": false,
        "autoWidth": true,
        "iDisplayLength": 10,
        "ordering": false,
        "bPaginate": false,
        "aoColumnDefs": [
        ]
    });

    function uploadCsvFile(event) {
        event.preventDefault();
        var data = new FormData($('form').get(0));
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function () {
                alert('Upload')
                    $('#import_csv').show();
                    $('#uploadCsvForm').hide();
            }
        });
    }
    $('#uploadCsvForm').submit(uploadCsvFile);

    $('#contacts_csv').click(function() {
        $.ajax({
            type: "GET",
            url: links.ajax_add_contact_csv,
            data: {},
            dataType: 'json',
            success: function (data) {
                $('#import_csv').hide();
                $('#uploadCsvForm').show();
                if (data['success'].length==0) {
                    alert('Not add contacts.');
                }
                else {
                    if (data['errors']==0) {
                        alert('All contacts add.');
                    }
                    else {
                        alert('Contacts added count - '+ data['success'].length+ '\nContact error - '+ data['errors']);
                    }
                    contactTable.api().ajax.reload(null, false);
                }
            },
        });
    });

    $('#contacts_vero').click(function() {
        $('#question').show();
        $('#ExportVero').show();
        $('#ModalVero').modal('show');
        $('#question').text('Are you sure that you want to export contacts to GetVero?');

    });

    $('#ExportVero').click(function() {
        $('#question').hide();
        $('#ExportVero').hide();
        $('#overlay').modal('show');
        $.ajax({
            type: "GET",
            url: links.ajax_add_contact_vero,
            data: {},
            dataType: 'json',
            success: function (data) {
                if (data['success']) {
                    $('#overlay').modal('hide');
                    $('#question').show();
                    $('#question').text('Contacts exported in GetVEro');
                    // alert('Contacts exported in GetVero');
                    contactTable.api().ajax.reload(null, false);
                }
            }
    });
    });
});

