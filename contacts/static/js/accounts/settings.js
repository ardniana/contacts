/**
 * Created by sasha on 31.05.17.
 */
$(document).ready(function () {
    $('#change_password').on('click', function () {
        $.ajax({
            url: links.ajax_change_password_form,
            type: 'GET',
            success: function (data) {
                bootbox.dialog({
                    message: data,
                    title: "Change Password",
                    buttons: {
                        "save": {
                            label: "Change Password",
                            callback: function () {
                                $.ajax({
                                    url: links.ajax_change_password_form,
                                    type: 'POST',
                                    data: {
                                        'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val(),
                                        'old_password': $("input[name='old_password']").val(),
                                        'new_password': $("input[name='new_password']").val(),
                                        'password_confirmation': $("input[name='password_confirmation']").val()
                                    },
                                    success: function (data) {
                                        var inputs = ['old_password', 'new_password', 'password_confirmation'];
                                        inputs.forEach(function (item, i, arr) {
                                            $('#' + item + '_div').removeClass('has-error');
                                            $('#' + item + '_help').text('');
                                        });
                                        if (data['success']) {
                                            bootbox.hideAll();
                                            bootbox.alert("Password successfully changed.");
                                        } else {
                                            data['errors'].forEach(function (item, i, arr) {
                                                $('#' + item[0] + '_div').addClass('has-error');
                                                $('#' + item[0] + '_help').text(item[1]);
                                            });
                                        }
                                    },
                                    failed: function () {
                                    }
                                });
                                return false;
                            }
                        }
                    },
                    callback: function (result) {
                    }
                });
            }
        });
});

});
